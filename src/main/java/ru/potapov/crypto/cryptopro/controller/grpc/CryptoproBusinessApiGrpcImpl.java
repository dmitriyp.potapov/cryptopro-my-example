package ru.potapov.crypto.cryptopro.controller.grpc;

import com.google.protobuf.BoolValue;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.potapov.crypto.cryptopro.api.ISignatureService;
import ru.potapov.crypto.cryptopro.util.Constant;
import ru.rb.api.grpc.crypto.cryptopro.v1.*;

import java.security.PrivateKey;

/**
 * @author Potapov Dmitry
 * @since 2020/04/16
 */
@Component
@GrpcService
public class CryptoproBusinessApiGrpcImpl extends CryptoproBusinessApiGrpc.CryptoproBusinessApiImplBase {
    private ISignatureService signatureService;

    @Autowired
    public void setSignatureService(ISignatureService signatureService) {
        this.signatureService = signatureService;
    }

    @Override
    public void sign(SignatureRequest request, StreamObserver<SignatureResponse> responseObserver) {
        byte[] signature = new byte[0];
        if ((request != null) && (!request.getTarget().isEmpty())
                && (!request.getAliasCert().isEmpty())){

            PrivateKey privateKey = null;
            try {
                privateKey = signatureService.getPrivateKey(Constant.KEY_STORE_FULL_PATH,
                        request.getAliasCert());

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (privateKey != null)
                signature = signatureService.sign(Constant.ALGORITHM_SIGNATURE, privateKey,
                        request.getTarget().toByteArray());
        }

        SignatureResponse responseBytes = SignatureResponse.newBuilder().setEncoded( ByteString.copyFrom(signature) ).build();
        responseObserver.onNext(responseBytes);
        responseObserver.onCompleted();
    }

    @Override
    public void checkSign(CheckSignRequest request, StreamObserver<CheckSignResponse> responseObserver) {
        boolean resultChecksign = false;
        if ((request != null) && (!request.getTarget().isEmpty())
                && (!request.getAliasCert().isEmpty())
                && (!request.getSign().isEmpty())){

            byte[] signature = request.getSign().toByteArray();

            try {
                resultChecksign = signatureService.verify(request.getAliasCert()
                        , request.getTarget().toByteArray(), signature);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        CheckSignResponse responseBoolean = CheckSignResponse.newBuilder().setValue(BoolValue.of(resultChecksign)).build();
        responseObserver.onNext(responseBoolean);
        responseObserver.onCompleted();
    }

    @Override
    public void validateCert(ValidateCertRequest request, StreamObserver<ValidateCertResponse> responseObserver) {
        boolean resultValidateCert = false;
        if ((request != null) && (!request.getEncoded().isEmpty())){

            resultValidateCert = signatureService.valid(request.getEncoded().toByteArray());
        }

        ValidateCertResponse responseBoolean = ValidateCertResponse.newBuilder().setValue(BoolValue.of(resultValidateCert)).build();
        responseObserver.onNext(responseBoolean);
        responseObserver.onCompleted();
    }
}
