package ru.potapov.crypto.cryptopro.controller.grpc;

import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.potapov.crypto.cryptopro.api.ISignatureService;
import ru.potapov.crypto.cryptopro.util.Constant;
import ru.rb.api.grpc.crypto.cryptopro.v1.*;

import java.security.PrivateKey;
import java.util.UUID;

/**
 * @author Potapov Dmitry
 * @since 2020/04/17
 */
@Component
@GrpcService
public class CryptoproAdminApiGrpcImpl extends CryptoproAdminApiGrpc.CryptoproAdminApiImplBase {
    private ISignatureService signatureService;

    @Autowired
    public void setSignatureService(ISignatureService signatureService) {
        this.signatureService = signatureService;
    }

    @Override
    public void createSign(Empty request, StreamObserver<AliasResponse> responseObserver) {
        String alias = UUID.randomUUID().toString(), resAlias = null;

        try {
            PrivateKey privateKey = signatureService.getPrivateKey(Constant.KEY_STORE_FULL_PATH, alias);
            if (privateKey == null){
                //получение сертификата и запись его в хранилище
                signatureService.writeCertSample(Constant.SIGN_KEY_PAIR_ALGALGORITHM, Constant.ALGORITHM_SIGNATURE,
                        alias, Constant.KEY_STORE_FULL_PATH, "CN="+alias+",  OU=Security, O=CryptoPro, C=RU",
                        true);
                privateKey = signatureService.getPrivateKey(Constant.KEY_STORE_FULL_PATH, alias);
            }
            byte[] signature = signatureService.sign(Constant.ALGORITHM_SIGNATURE
                    , privateKey
                    , Constant.TARGET.getBytes());

            final boolean signELver = signatureService.verify(alias, Constant.TARGET.getBytes(), signature);

            if (!signatureService.verify(alias, Constant.TARGET.getBytes(), signature))
                alias = "";
            resAlias = alias;
        } catch (Exception e) {
            e.printStackTrace();
        }

        AliasResponse responseBytes = AliasResponse.newBuilder().setValue(StringValue.of(resAlias)).build();
        responseObserver.onNext(responseBytes);
        responseObserver.onCompleted();
    }

    @Override
    public void putCert(PutCertRequest request, StreamObserver<PutCertResponse> responseObserver) {
        String alias = null;
        boolean resultChecksign = false;
        if ((request != null) && (!request.getCert().isEmpty())){
            alias = signatureService.putCert(request.getCert().toByteArray());
        }

        PutCertResponse responsePutCert = PutCertResponse.newBuilder().setValue(StringValue.of(alias)).build();
        responseObserver.onNext(responsePutCert);
        responseObserver.onCompleted();
    }

    @Override
    public void uploadSign(UploadSignRequest request, StreamObserver<AliasResponse> responseObserver) {
        String resAlias = null;
        if((request != null) && (!request.getValue().isEmpty())){

            resAlias = signatureService.downloadCert(request.getValue());
        }

        AliasResponse responseBytes = AliasResponse.newBuilder().setValue(StringValue.of(resAlias)).build();
        responseObserver.onNext(responseBytes);
        responseObserver.onCompleted();
    }
}
