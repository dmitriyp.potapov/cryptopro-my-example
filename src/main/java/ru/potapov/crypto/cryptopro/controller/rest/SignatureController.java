package ru.potapov.crypto.cryptopro.controller.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.potapov.crypto.cryptopro.api.ISignatureService;
import ru.potapov.crypto.cryptopro.dto.RequestForSignature;
import ru.potapov.crypto.cryptopro.dto.RequestForChecksign;
import ru.potapov.crypto.cryptopro.dto.RequestForValidateCert;
import ru.potapov.crypto.cryptopro.util.Constant;

import java.security.PrivateKey;
import java.util.UUID;

@RestController
@Api("Signature main rest controller")
public class SignatureController {
    private ISignatureService signatureService;

    public ISignatureService getSignatureService() {
        return signatureService;
    }

    @Autowired
    public void setSignatureService(final ISignatureService signatureService) {
        this.signatureService = signatureService;
    }

    @ApiOperation(
            value = "Создание подписи",
            notes = "Создание подписи")
    @PostMapping("/createsign")
    public String createSign() throws Exception {
        String alias = UUID.randomUUID().toString();

        PrivateKey privateKey = signatureService.getPrivateKey(Constant.KEY_STORE_FULL_PATH, alias);
        if (privateKey == null){
            //получение сертификата и запись его в хранилище
            signatureService.writeCertSample(Constant.SIGN_KEY_PAIR_ALGALGORITHM, Constant.ALGORITHM_SIGNATURE,
                    alias, Constant.KEY_STORE_FULL_PATH, "CN="+alias+",  OU=Security, O=CryptoPro, C=RU",
                    true);
            privateKey = signatureService.getPrivateKey(Constant.KEY_STORE_FULL_PATH, alias);
        }
        byte[] signature = signatureService.sign(Constant.ALGORITHM_SIGNATURE, privateKey, Constant.TARGET.getBytes());

        final boolean signELver = signatureService.verify(alias, Constant.TARGET.getBytes(), signature);

        if (!signatureService.verify(alias, Constant.TARGET.getBytes(), signature))
            alias = "";

        return alias;
    }

    @ApiOperation(
            value = "Подпись",
            notes = "Подпись")
    @PostMapping("/sign")
    public String sign(@RequestBody RequestForSignature requestForSignature) throws Exception {
        if ((requestForSignature == null) || "".equals(requestForSignature.getTarget())){
            return null;
        }

        PrivateKey privateKey = signatureService.getPrivateKey(Constant.KEY_STORE_FULL_PATH, requestForSignature.getAliasCert());

        byte[] signature = null;
        if (privateKey != null)
           signature = signatureService.sign(Constant.ALGORITHM_SIGNATURE, privateKey, requestForSignature.getTarget().getBytes());

        return Constant.toHexString(signature);
    }

    @ApiOperation(
            value = "Проверка подписи",
            notes = "Проверка подписи")
    @PostMapping("/checksign")
    public boolean checkSign(@RequestBody RequestForChecksign requestForChecksign) throws Exception {
        if ((requestForChecksign == null) || "".equals(requestForChecksign.getTarget())){
            return false;
        }

        byte[] encoded = Constant.hexStringToByteArray(requestForChecksign.getSign());

        final boolean signELver = signatureService.verify(requestForChecksign.getAliasCert()
                , requestForChecksign.getTarget().getBytes()
                , encoded);

        return signELver;
    }

    @ApiOperation(
            value = "Загрузка сертификата",
            notes = "Загрузка сертификата из файла на диске")
    @PostMapping("/download/{alias}")
    public boolean downloadSign(@RequestParam String alias) throws Exception {
        signatureService.downloadCert(alias);
        return true;
    }

    @ApiOperation(
            value = "Загрузка сертификата",
            notes = "Загрузка сертификата")
    @PostMapping("/putcert/{alias}")
    public String putCert(@RequestParam String cert) throws Exception {
        return signatureService.putCert(cert);
    }

    @ApiOperation(
            value = "Получить сертификат",
            notes = "Получить сертификат")
    @PostMapping("/getcert/{alias}")
    public byte[] getCert(@RequestParam String cert) throws Exception {
        return signatureService.getCert(cert);
    }

    @ApiOperation(
            value = "Проверка сертификата",
            notes = "Проверка сертификата")
    @PostMapping("/validateCert/{alias}")
    public boolean validateCert(@RequestBody RequestForValidateCert cert) throws Exception {
        if (cert != null)
            return signatureService.valid(cert.getCertificate());

        return false;
    }
}
