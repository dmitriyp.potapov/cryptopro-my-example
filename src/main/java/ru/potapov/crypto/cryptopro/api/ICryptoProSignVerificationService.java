package ru.potapov.crypto.cryptopro.api;

import ru.potapov.crypto.cryptopro.dto.svs.SignersInfo;
import ru.potapov.crypto.cryptopro.dto.svs.VerificationResultRest;
import ru.potapov.crypto.cryptopro.dto.svs.VsPolicyRest;

import java.util.Optional;

/**
 * @author Potapov Dmitry
 * @since 2020/05/22
 */
public interface ICryptoProSignVerificationService {
    Optional<VsPolicyRest> getPolicy();
    Optional<Boolean> verifyCert(byte[] encoded);
    Optional<VerificationResultRest> checkSign(byte[] target, byte[] certificate, byte[] sign );
    Optional<SignersInfo> signersInfo(byte[] target, byte[] certificate, byte[] sign);
}
