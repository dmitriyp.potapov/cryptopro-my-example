package ru.potapov.crypto.cryptopro.api;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author Potapov Dmitry
 * @since 2020/03/28
 */
public interface ISignatureService {
    byte[] sign(String algoritm, PrivateKey privateKey,byte[] target);
    boolean checkSign(String algoritm, PublicKey publicKey, byte[] signature, byte[] target);
    PrivateKey getPrivateKey(String storePath, String alias) throws Exception;
    PublicKey getPublicKey(String storePath, String alias) throws Exception;
    KeyPair getKeyPair();
    boolean verify(String alghorithmName, byte[] data, byte[] signature) throws Exception;
    void writeCertSample(String keyAlg, String signAlg,
                         String alias, String storePath, String dnName, boolean forDocker) throws Exception;
    String downloadCert(String alias);
    String putCert(String cert);
    byte[] getCert(String cert);
    String putCert(byte[] encoded);
    boolean valid(String cert);
    boolean valid(byte[] encoded);
}
