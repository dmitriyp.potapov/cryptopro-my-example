package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignersInfo implements Serializable {
    /**
     *Информация о найденных подписях
     */  @JsonProperty("SignerInfoList")
    private List<SignerInfo> signerInfoList;

    /**
     *Дополнительная информация о подписанном документе.
     */
    @JsonProperty("AdditionalInfo")
    private AdditionalSignedDocumentInfo additionalInfo;

    public SignersInfo() {}

    public List<SignerInfo> getSignerInfoList() {
        return signerInfoList;
    }

    public void setSignerInfoList(List<SignerInfo> signerInfoList) {
        this.signerInfoList = signerInfoList;
    }

    public AdditionalSignedDocumentInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(AdditionalSignedDocumentInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString() {
        return "SignersInfo{" + "\n" +
                "   SignerInfo=" + signerInfoList + "\n" +
                "   AdditionalSignedDocumentInfo=" + additionalInfo + "\n" +
                "}";
    }
}
