package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Certificate implements Serializable {
    /**
     *Сертификат.
     */
    @JsonProperty("Content")
    private byte[] content;

    /**
     *Список идентификаторов плагинов для дополнительных проверок сертификата.
     * Если CertVerifiersPluginsIds не передан в запросе или явно
     * указан как NULL, то при проверке будут использоваться
     * плагины, у которых CheckByDefaultRequired = true.
     * Если CertVerifiersPluginsIds проинициализирован, то для
     * проверки будут использованы плагины с указанными
     * идентификаторами. Если список пустой — дополнительная
     * проверка не выполняется.
     */
    @JsonProperty("CertVerifiersPluginsIds")
    private List<Integer> certVerifiersPluginsIds;

    public Certificate() {
    }

    public Certificate(byte[] content) { this.content = content; }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public List<Integer> getCertVerifiersPluginsIds() {
        return certVerifiersPluginsIds;
    }

    public void setCertVerifiersPluginsIds(List<Integer> certVerifiersPluginsIds) {
        this.certVerifiersPluginsIds = certVerifiersPluginsIds;
    }

    @Override
    public String toString() {
        return "Certificate{" + "\n" +
                "   Content=" + content + "\n" +
                "   CertVerifiersPluginsIds=" + certVerifiersPluginsIds + "\n" +
                "}";
    }
}
