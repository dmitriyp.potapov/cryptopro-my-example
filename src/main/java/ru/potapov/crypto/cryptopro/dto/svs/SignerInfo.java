package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.potapov.crypto.cryptopro.dto.svs.enumarate.CertificateInfoParams;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignerInfo implements Serializable {
    /**
     *Идентификатор узла подписи.
     */
    @JsonProperty("Id")
    private String id;

    /**
     *Идентификатор родительского узла подписи.
     */
    @JsonProperty("ParentId")
    private String parentId;

    /**
     *Порядковый номер узла
     * подписи. Порядковый номер начинается с 1.
     */
    @JsonProperty("Index")
    private int index;

    /**
     *Отображаемые данные о сертификате.
     */
    @JsonProperty("SignerCertificateInfo")
    private Map<CertificateInfoParams, String> SignerCertificateInfo;

    public SignerInfo() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Map<CertificateInfoParams, String> getSignerCertificateInfo() {
        return SignerCertificateInfo;
    }

    public void setSignerCertificateInfo(Map<CertificateInfoParams, String> signerCertificateInfo) {
        SignerCertificateInfo = signerCertificateInfo;
    }

    @Override
    public String toString() {
        return "SignerInfo{" + "\n" +
                "   Id=" + id + "\n" +
                "   ParentId=" + parentId + "\n" +
                "   Index=" + index + "\n" +
                "}";
    }
}
