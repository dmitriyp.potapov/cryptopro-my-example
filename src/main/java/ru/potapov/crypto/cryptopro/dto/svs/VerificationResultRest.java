package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.potapov.crypto.cryptopro.dto.svs.enumarate.CertificateInfoParams;
import ru.potapov.crypto.cryptopro.dto.svs.enumarate.SignatureInfoParams;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VerificationResultRest implements Serializable {
    /**
     *Суммарная информация о результатах проверки подписи.
     */
    @JsonProperty("Message")
    private String message;

    /**
     *Результат проверки подписи или сертификата.
     */
    @JsonProperty("Result")
    private boolean result;

    /**
     *Сертификат.
     */
    @JsonProperty("SignerCertificate")
    private byte[] signerCertificate;

    /**
     *Набор сведений о подписанте.
     */
    @JsonProperty("SignerCertificateInfo")
    private Map<CertificateInfoParams, String> signerCertificateInfo;

    /**
     *Дополнительные сведения о подписи.
     */
    @JsonProperty("SignatureInfo")
    private Map<SignatureInfoParams, String> signatureInfo;

    /**
     *Дополнительная информация о подписанном документе.
     */
    @JsonProperty("AdditionalCertificateResult")
    private String additionalCertificateResult;

    public VerificationResultRest() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public byte[] getSignerCertificate() {
        return signerCertificate;
    }

    public void setSignerCertificate(byte[] signerCertificate) {
        this.signerCertificate = signerCertificate;
    }

    public Map<CertificateInfoParams, String> getSignerCertificateInfo() {
        return signerCertificateInfo;
    }

    public void setSignerCertificateInfo(Map<CertificateInfoParams, String> signerCertificateInfo) {
        this.signerCertificateInfo = signerCertificateInfo;
    }

    public Map<SignatureInfoParams, String> getSignatureInfo() {
        return signatureInfo;
    }

    public void setSignatureInfo(Map<SignatureInfoParams, String> signatureInfo) {
        this.signatureInfo = signatureInfo;
    }

    public String getAdditionalCertificateResult() {
        return additionalCertificateResult;
    }

    public void setAdditionalCertificateResult(String additionalCertificateResult) {
        this.additionalCertificateResult = additionalCertificateResult;
    }
}
