package ru.potapov.crypto.cryptopro.dto;

import org.springframework.stereotype.Component;

@Component
public class RequestForSignature {
    private String target;
    private String aliasCert;

    public RequestForSignature() {
    }

    public RequestForSignature(RequestForSignature requestForSignature) {
        this.target = requestForSignature.target;
        this.aliasCert = requestForSignature.aliasCert;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getAliasCert() {
        return aliasCert;
    }

    public void setAliasCert(String aliasCert) {
        this.aliasCert = aliasCert;
    }

}
