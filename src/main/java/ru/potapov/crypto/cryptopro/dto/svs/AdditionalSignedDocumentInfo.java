package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
public class AdditionalSignedDocumentInfo implements Serializable {
    /**
     * Содержимое присоединенной подписи в формате CMS.
     */
    @JsonProperty("Content")
    private byte[] content;

    public AdditionalSignedDocumentInfo() { }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "AdditionalSignedDocumentInfo{" + "\n" +
                "   Content=" + content + "\n" +
                "}";
    }
}
