package ru.potapov.crypto.cryptopro.dto.svs.enumarate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum VerifyParams  implements Serializable {
    /**
     *В качестве данных для проверки подписи передаётся хэш-значение от
     * исходного документа.
     */
    @JsonProperty("Hash")
    Hash("Hash"),

    /**
     *Идентификатор подписи.
     */
    @JsonProperty("SignatureId")
    SignatureId("SignatureId"),

    /**
     *Порядковый номер подписи (начиная с 1).
     */
    @JsonProperty("SignatureIndex")
    SignatureIndex("SignatureIndex"),

    /**
     *Проверить все подписи в документе.
     */
    @JsonProperty("VerifyAll")
    VerifyAll("VerifyAll"),

    /**
     *Идентификатор алгоритма хеширования.
     * Допустимые значения:
     *  GOST R 34.11-94
     *  GR 34.11-2012 256
     * HashAlgorithm
     *  GR 34.11-2012 512
     *  SHA-1
     *  SHA-256
     *  SHA-384
     *  SHA-512
     *  MD5
     */
    @JsonProperty("HashAlgorithm")
    HashAlgorithm("HashAlgorithm"),

    /**
     *Флаг, указывающий на то, что в параметре document передан PKCS#7
     */
    @JsonProperty("VerifyPKCS7")
    VerifyPKCS7("VerifyPKCS7"),

    /**
     *Флаг, указывающий на то, что необходимо вернуть Content
     * присоединенной CMS-подписи
     */
    @JsonProperty("ExtractContent")
    ExtractContent("ExtractContent");

    private String title;


    VerifyParams() { }

    VerifyParams(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
