package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VsPolicyRest implements Serializable {
    /**
     *Форматы подписи, которые можно проверить.
     */
    @JsonProperty("SignatureDescriptions")
    private List<ru.potapov.crypto.cryptopro.dto.svs.SignatureTypeDescription> signatureDescriptions;

    /**
     *Зарегистрированные плагины для
     * дополнительных проверок сертификатов.
     */
    @JsonProperty("CertificateVerifiers")
    private List<CertificateVerifier> certificateVerifiers;

    public VsPolicyRest() {
    }

    public List<ru.potapov.crypto.cryptopro.dto.svs.SignatureTypeDescription> getSignatureDescriptions() {
        return signatureDescriptions;
    }

    public void setSignatureDescriptions(List<ru.potapov.crypto.cryptopro.dto.svs.SignatureTypeDescription> signatureDescriptions) {
        this.signatureDescriptions = signatureDescriptions;
    }

    public List<CertificateVerifier> getCertificateVerifiers() {
        return certificateVerifiers;
    }

    public void setCertificateVerifiers(List<CertificateVerifier> certificateVerifiers) {
        this.certificateVerifiers = certificateVerifiers;
    }

    @Override
    public String toString() {
        return "VsPolicyRest{" + "\n" +
                "   signatureDescriptions='" + signatureDescriptions + "\n" +
                "   certificateVerifiers=" + certificateVerifiers + "\n" +
                "}";
    }
}
