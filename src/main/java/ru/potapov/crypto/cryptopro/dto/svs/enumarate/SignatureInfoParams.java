package ru.potapov.crypto.cryptopro.dto.svs.enumarate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum SignatureInfoParams implements Serializable {
    /**
     * Формат подписи CAdES.
     * Возможные значения: BES, T, XLT1
     */
    @JsonProperty("CAdESType")
    CAdESType("CAdESType"),

    /**
     *Время (UTC) подписи из штампа времени на подпись.
     */
    @JsonProperty("SigningTime")
    SigningTime("SigningTime"),

    /**
     *Время (UTC) подписи по локальным часам компьютера, на котором
     * была создана подпись. Значение берётся из атрибута SigningTime.
     */
    @JsonProperty("LocalSigningTime")
    LocalSigningTime("LocalSigningTime");

    private String title;

    SignatureInfoParams() {
    }

    SignatureInfoParams(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
