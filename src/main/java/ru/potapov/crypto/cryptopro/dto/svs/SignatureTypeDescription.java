package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.potapov.crypto.cryptopro.dto.svs.enumarate.SignatureType;

import java.io.Serializable;
import java.util.List;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignatureTypeDescription implements Serializable {
    /**
     *Перечисление, содержащее
     * возможные форматы подписи.
     */
    @JsonProperty("SignatureType")
    private SignatureType signatureType;

    /**
     *Связанные расширения файлов.
     */
    @JsonProperty("FileExtensions")
    private List<String> fileExtensions;

    public SignatureTypeDescription() {
    }

    public SignatureType getSignatureType() {
        return signatureType;
    }

    public void setSignatureType(SignatureType signatureType) {
        this.signatureType = signatureType;
    }

    public List<String> getFileExtensions() {
        return fileExtensions;
    }

    public void setFileExtensions(List<String> fileExtensions) {
        this.fileExtensions = fileExtensions;
    }

    @Override
    public String toString() {
        return "SignatureTypeDescription{"+ "\n" +
                "   signatureType='" + signatureType + "\n"  +
                "   fileExtensions=" + fileExtensions + "\n" +
                '}';
    }
}
