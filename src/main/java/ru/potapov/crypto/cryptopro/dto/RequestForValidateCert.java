package ru.potapov.crypto.cryptopro.dto;

import org.springframework.stereotype.Component;

@Component
public class RequestForValidateCert {
    private String certificate;

    public RequestForValidateCert() {
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }
}
