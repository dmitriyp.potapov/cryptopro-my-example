package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.potapov.crypto.cryptopro.dto.svs.enumarate.SignatureType;
import ru.potapov.crypto.cryptopro.dto.svs.enumarate.VerifyParams;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignedDocument implements Serializable {
    /**
     *Формат подписи SignatureType.
     * Обязательный параметр.
     */
    @JsonProperty("SignatureType")
    private SignatureType signatureType = SignatureType.GOST3410;

    /**
     *Исходный документ. Используется только
     * для проверки подписи формата GOST3410 и
     * отделенной подписи форматов CMS и
     * CAdES.
     */
    @JsonProperty("Source")
    private byte[] source;

    /**
     *Сертификат подписанта.
     * Используется только для проверки подписи
     * формата GOST3410.
     */
    @JsonProperty("Certificate")
    private byte[] certificate;

    /**
     *Подписанный документ или значение
     * подписи для проверки. Обязательный
     * параметр.
     */
    @JsonProperty("Content")
    private byte[] content;

    /**
     *Словарь дополнительных параметров
     * проверки подписи. Необязательный
     * параметр.
     */
    @JsonProperty("VerifyParams")
    private Map<VerifyParams, String> verifyParams = new HashMap<>();

    /**
     *Список идентификаторов плагинов для
     * дополнительных проверок сертификата,
     * используемого при подписи.
     * Если CertVerifiersPluginsIds не передан в
     * запросе или явно указан как NULL, то при
     * проверке будут использоваться плагины, у
     * которых CheckByDefaultRequired = true.
     * Если CertVerifiersPluginsIds
     * проинициализирован, то для проверки
     * будут использованы плагины с указанными
     * идентификаторами. Если список пустой —
     * дополнительная проверка не выполняется.
     */
    @JsonProperty("CertVerifiersPluginsIds")
    private List<Integer> certVerifiersPluginsIds = new ArrayList<>();

    public SignedDocument() {}

    public SignatureType getSignatureType() {
        return signatureType;
    }

    public void setSignatureType(SignatureType signatureType) {
        this.signatureType = signatureType;
    }

    public byte[] getSource() {
        return source;
    }

    public void setSource(byte[] source) {
        this.source = source;
    }

    public byte[] getCertificate() {
        return certificate;
    }

    public void setCertificate(byte[] certificate) {
        this.certificate = certificate;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Map<VerifyParams, String> getVerifyParams() {
        return verifyParams;
    }

    public void setVerifyParams(Map<VerifyParams, String> verifyParams) {
        this.verifyParams = verifyParams;
    }

    public List<Integer> getCertVerifiersPluginsIds() {
        return certVerifiersPluginsIds;
    }

    public void setCertVerifiersPluginsIds(List<Integer> certVerifiersPluginsIds) {
        this.certVerifiersPluginsIds = certVerifiersPluginsIds;
    }


}
