package ru.potapov.crypto.cryptopro.dto.svs.enumarate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum CertificateInfoParams implements Serializable {
    /**
     *X500 имя субъекта сертификата
     */
    @JsonProperty("SubjectName")
    SubjectName("SubjectName"),

    /**
     *X500 имя издателя сертификата
     */
    @JsonProperty("IssuerName")
    IssuerName("IssuerName"),

    /**
     *Окончание срока действия сертификата
     */
    @JsonProperty("NotAfter")
    NotAfter("NotAfter"),

    /**
     *Начало срока действия сертификата
     */
    @JsonProperty("NotBefore")
    NotBefore("NotBefore"),

    /**
     *Серийный номер сертификата
     */
    @JsonProperty("SerialNumber")
    SerialNumber("SerialNumber"),

    /**
     *Отпечаток сертификата
     */
    @JsonProperty("Thumbprint")
    Thumbprint("Thumbprint");

    private String title;

    CertificateInfoParams() { }

    CertificateInfoParams(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
