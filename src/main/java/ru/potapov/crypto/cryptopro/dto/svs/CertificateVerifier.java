package ru.potapov.crypto.cryptopro.dto.svs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CertificateVerifier  implements Serializable {
    /**
     *Идентификатор плагина.
     */
    @JsonProperty("ID")
    private int id;

    /**
     *Имя класса, который реализует интерфейс ISVSCertificateVerifier.
     */
    @JsonProperty("ClassName")
    private String className;

    /**
     *Полный путь до файла со сборкой плагина.
     * В качестве значения данного параметра можно
     * указать полный путь до файла со сборкой, либо
     * только имя dll-файла сборки, если плагин
     * находится в следующей директории: <Путь
     * установки>\Plugins\CertificatesVerifiers
     */
    @JsonProperty("AssemblyName")
    private String assemblyName;

    /**
     *Описание плагина, которое отображается на
     * Веб-интерфейсе Сервиса Проверки Подписи.
     */
    @JsonProperty("PluginDescription")
    private String pluginDescription;

    /**
     *Использовать ли по умолчанию плагин для
     * проверки сертификата.
     */
    @JsonProperty("CheckByDefaultRequired")
    private boolean checkByDefaultRequired;

    /**
     *Дополнительные настройки плагина.
     */
    @JsonProperty("Parameters")
    private Map<String, String> parameters;

    public CertificateVerifier() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyName(String assemblyName) {
        this.assemblyName = assemblyName;
    }

    public String getPluginDescription() {
        return pluginDescription;
    }

    public void setPluginDescription(String pluginDescription) {
        this.pluginDescription = pluginDescription;
    }

    public boolean isCheckByDefaultRequired() {
        return checkByDefaultRequired;
    }

    public void setCheckByDefaultRequired(boolean checkByDefaultRequired) {
        this.checkByDefaultRequired = checkByDefaultRequired;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "CertificateVerifier{"+ "\n" +
                "   id='" + id + "\n"  +
                "   className='" + className + "\n" +
                "   assemblyName='" + assemblyName + "\n" +
                "   pluginDescription='" + pluginDescription+ "\n" +
                "   checkByDefaultRequired='" + checkByDefaultRequired + "\n" +
                "   parameters=" + parameters + "\n" +
                '}';
    }
}
