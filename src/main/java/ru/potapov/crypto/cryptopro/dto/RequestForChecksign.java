package ru.potapov.crypto.cryptopro.dto;

import org.springframework.stereotype.Component;

@Component
public class RequestForChecksign {
    private String target;
    private String sign; //in HEX string like  b9 8f 58 5c 21 6d 57 20 b5 f0 5d 23 8c 65 6b 75 62 e5 33 1d 32 c2 62 12 e.c.
    private String aliasCert;

    public RequestForChecksign() {}

    public RequestForChecksign(RequestForChecksign requestForChecksign) {
        this.target = requestForChecksign.target;
        this.sign = requestForChecksign.sign;
        this.aliasCert = requestForChecksign.aliasCert;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getAliasCert() {
        return aliasCert;
    }

    public void setAliasCert(String aliasCert) {
        this.aliasCert = aliasCert;
    }
}
