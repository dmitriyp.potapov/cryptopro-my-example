package ru.potapov.crypto.cryptopro.dto.svs.enumarate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Potapov Dmitry
 * @since 2020/04/26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum SignatureType implements Serializable {

    /**
     *Подпись документа в формате XMLDSig
     */
    @JsonProperty("XMLDSig")
    XMLDSig("XMLDSig"),

    /**
     *Электронная подпись по ГОСТ Р 34.10– 2001
     * и ГОСТ Р 34.10– 2012
     */
    @JsonProperty("GOST3410")
    GOST3410("GOST3410"),

    /**
     *Подпись формата CAdES
     * Возможные значения: BES, T, XLT1
     */
    @JsonProperty("CAdES")
    CAdES("CAdES"),

    /**
     *Подпись PDF документов
     */
    @JsonProperty("PDF")
    PDF("PDF"),

    /**
     *Подпись документов MS Word и Excel
     */
    @JsonProperty("MSOffice")
    MSOffice("MSOffice"),

    /**
     *Подпись формата CAdES-BES
     */
    @JsonProperty("CMS")
    CMS("CMS");

    private String title;

    SignatureType() {
    }

    SignatureType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
