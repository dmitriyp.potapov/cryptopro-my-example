package ru.potapov.crypto.cryptopro.config;

import io.grpc.health.v1.HealthCheckResponse;
import io.grpc.services.HealthStatusManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Slf4j
@Configuration
@EnableConfigurationProperties
public class AppConfig {
    @Bean
    public HealthStatusManager healthStatusManager()  {
        HealthStatusManager manager = new HealthStatusManager();
        manager.setStatus("", HealthCheckResponse.ServingStatus.NOT_SERVING);
        return manager;
    }

    @EventListener
    public void onContextRefreshed(ContextRefreshedEvent event) {
        log.debug("onContextRefreshed(event: {})...", event);
        healthStatusManager().setStatus("", HealthCheckResponse.ServingStatus.SERVING);
        log.info("Crypto-pro service is ready");
    }
}
