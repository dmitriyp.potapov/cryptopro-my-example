package ru.potapov.crypto.cryptopro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Potapov Dmitry
 * @since 2020/03/28
 */
@SpringBootApplication
public class CryptoProDriverApp {
    public static void main(String[] args) {
        SpringApplication.run(CryptoProDriverApp.class, args);
    }
}
