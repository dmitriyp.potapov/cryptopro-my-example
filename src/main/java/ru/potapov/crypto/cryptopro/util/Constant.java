package ru.potapov.crypto.cryptopro.util;

import org.springframework.stereotype.Component;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;

import java.io.File;

@Component
public class Constant {
    public static final String TARGET = "Check for sign";
    public static final String HTTP_ADDRESS="http://www.cryptopro.ru/certsrv/";

    /**
     * для объекта Signature
     * алгоритм подписи ГОСТ Р 34.10-2012 (512): "GOST3411_2012_512withGOST3410_2012_512" или
     * JCP.GOST_SIGN_2012_512_NAME
     */
    public static final String ALGORITHM_SIGNATURE = JCP.GOST_SIGN_2012_512_NAME; //GOST3411_2012_512withGOST3410_2012_512

    /**
     * для генератора запросов
     * алгоритм ключа подписи: "GOST3410_2012_512" или JCP.GOST_EL_2012_512_NAME
     */
    public static final String SIGN_KEY_PAIR_ALGALGORITHM = JCP.GOST_EL_2012_512_NAME;
    /**
     * стандарт сертификата "X509" или JCP.CERTIFICATE_FACTORY_NAME
     */
    public static final String CF_ALG = JCP.CERTIFICATE_FACTORY_NAME;//"X509"; {"X.509"}
    public static final char[] STORE_PASS = new char[] {'1'};

    public static final String TRUST_STORE_PATH = "/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security";
    public static final String TRUST_STORE_FULL_PATH = TRUST_STORE_PATH + File.separator + "cacerts";
    public static final String KEY_STORE_PATH = System.getProperty("user.home") + File.separator;//HDImageStore.getDir();// System.getProperty("user.home")+ File.separator+"ca-certificates" + File.separator;
    public static final String KEY_STORE_FULL_PATH = KEY_STORE_PATH + "storage_2012_512.keystore";
    public static final String CRL_FULL_PATH = KEY_STORE_PATH + File.separator + "crls";

    public static final String ALIAS_CERT="techic3_certificate_2012_512";
    public static final String DNNAME_CERT ="CN="+ALIAS_CERT+",  OU=Security, O=CryptoPro, C=RU";

    /**
     * тип хранилища:
     * <p/>
     * "HDImageStore" - жесткий диск
     * <p/>
     * "FloppyStore" - дискета, флешка
     * <p/>
     * "OCFStore" или "J6CFStore" - карточка
     */
    public static final String CERT_STORE_NAME_JCSP = JCSP.CERT_STORE_NAME;
    public static final String KEYSTORE_TYPE_JCSP = JCSP.HD_STORE_NAME;//JCSP.REG_STORE_NAME;//JCSP.HD_STORE_NAME;
    public final static String PROVIDER_NAME_JCSP = JCSP.PROVIDER_NAME;


    /**
     * hex-string
     *
     * @param array массив данных
     * @return hex-string
     */
    public static String toHexString(byte[] array) {
        if (array == null)
            return null;

        StringBuffer ss = new StringBuffer(array.length * 3);
        for (int i = 0; i < array.length; i++) {
            ss.append(' ');
            ss.append(Character.forDigit((array[i] >> 4) & 0xF, 16));
            ss.append(Character.forDigit((array[i] & 0xF), 16));
        }

        return ss.toString();
    }

    /**
     * hex-string
     *
     * @param s hex-string for convert to array[]
     * @return byte[] - result of converting hex-string to array[]
     */
    public static byte[] hexStringToByteArray(String s) {
        if (s == null)
            return null;

        s = s.replace(" ", "");
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    //SVS

    /**
     * SVS-сервер.
     * REST: URI
     */
    public final static String SVS_REST = "http://simdss.cryptopro.ru/verify/rest";

    /**
     * SVS-сервер.
     * REST: конечная точка Policy
     * Данная конечная точка позволяет получить доступ к
     * политике (настройкам) Сервиса Проверки Подписи.
     */
    public final static String SVS_REST_POLICY = "/api/policy";

    /**
     * SVS-сервер.
     * REST: конечная точка Certificates
     * Данная конечная точка позволяет получить доступ к функции,
     * осуществляющей проверку действительности сертификата.
     */
    public final static String SVS_REST_CERTIFICATE = "/api/certificates";

    /**
     * SVS-сервер.
     * REST: конечная точка Signatures
     * Данная конечная точка позволяет получить доступ к функции,
     * осуществляющей проверку ЭП документа.
     */
    public final static String SVS_REST_SIGNATURES = "/api/signatures";

    /**
     * SVS-сервер.
     * REST: конечная точка SignersInfo
     * Получение информации о подписанте.
     */
    public final static String SVS_REST_SIGNERS_INFO = "/api/signatures/signersInfo";
}
