package ru.potapov.crypto.cryptopro.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.potapov.crypto.cryptopro.api.ICryptoProSignVerificationService;
import ru.potapov.crypto.cryptopro.api.ICryptoProSignVerificationService;
import ru.potapov.crypto.cryptopro.dto.svs.*;
import ru.potapov.crypto.cryptopro.util.Constant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Optional;

/**
 * Сервис для работы с крипто-про SVS
 * @author Potapov Dmitry
 * @since 2020/05/22
 */
@Service
public class CryptoProSignVerificationService implements ICryptoProSignVerificationService {
    private SignatureService signatureService;

    @Autowired
    public void setSignatureService(SignatureService signatureService) {
        this.signatureService = signatureService;
    }

    /**
     * Метод проверки сертификата
     *
     * @param encoded массив данных
     * @return Optional<Boolean>
     */
    @Override
    public Optional<Boolean> verifyCert(byte[] encoded) {
        Optional<Boolean> result = Optional.empty();
        if (encoded == null){
            return result;
        }

        Certificate certificate = new Certificate();
        certificate.setContent(encoded);

        WebClient webClient = WebClient
                .builder()
                    .baseUrl(Constant.SVS_REST)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();


        VerificationResultRest verificationResultRest = null;
        try {
            verificationResultRest = webClient.post()
                                        .uri(Constant.SVS_REST_CERTIFICATE)
                                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                        .body(Mono.just(certificate), Certificate.class)
                                        .retrieve()
                                        .bodyToMono(VerificationResultRest.class)
                                        .block();

        } catch (Exception e) {
            e.printStackTrace();
        }

        result = Optional.of(verificationResultRest.isResult());

        return result;
    }

    /**
     * Метод запроса политики SVS-сервера
     *
     * @return Optional<VsPolicyRest>
     */
    @Override
    public Optional<VsPolicyRest> getPolicy(){
        Optional<VsPolicyRest> resultOptional = Optional.empty();
        WebClient webClient = WebClient.create(Constant.SVS_REST);
        VsPolicyRest vsPolicyRest = webClient.get().uri(Constant.SVS_REST_POLICY).retrieve().bodyToMono(VsPolicyRest.class).block();
        resultOptional = Optional.of(vsPolicyRest);

        return resultOptional;
    }

    /**
     * Метод позволяет получить информацию о подписанте.
     *  На момент написания класса, адрес не работал.
     *
     * @param target массив данных
     *          Исходный документ. Используется только
     *          для проверки подписи формата GOST3410 и
     *          отделенной подписи форматов CMS и CAdES.
     *
     * @param certificate массив данных
     *          Сертификат подписанта.
     *          Используется только для проверки подписи
     *          формата GOST3410.
     *
     * @param sign массив данных
     *          Подписанный документ или значение
     *          подписи для проверки.
     * @return Optional<SignersInfo>
     */
    @Override
    public Optional<SignersInfo> signersInfo(byte[] target, byte[] certificate, byte[] sign){
        Optional<SignersInfo> result = Optional.empty();

        SignedDocument signedDocument =  new SignedDocument();
        signedDocument.setSource(target);
        signedDocument.setCertificate(certificate);
        signedDocument.setContent(sign);

        BodyInserter<Object, ReactiveHttpOutputMessage> inserter3
                = BodyInserters.fromObject(signedDocument);

        WebClient client = WebClient
                .builder()
                    .baseUrl(Constant.SVS_REST)
                    .defaultCookie("cookieKey", "cookieValue")
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .defaultUriVariables(Collections.singletonMap("url", Constant.SVS_REST))
                .build();


        WebClient.RequestBodySpec uri1 = client
                .method(HttpMethod.POST)
                .uri(Constant.SVS_REST_SIGNERS_INFO);

        WebClient.ResponseSpec response1 = uri1
                .body(inserter3)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                .acceptCharset(Charset.forName("UTF-8"))
                .ifNoneMatch("*")
                .ifModifiedSince(ZonedDateTime.now())
                .retrieve();

        SignersInfo signersInfo = response1
                .bodyToMono(SignersInfo.class)
                .block();

        result = Optional.of(signersInfo);

        return result;
    }

    /**
     * Метод позволяет получить доступ
     * к функции, осуществляющей проверку
     * ЭП документа.
     *
     * @param target массив данных
     *          Исходный документ. Используется только
     *          для проверки подписи формата GOST3410 и
     *          отделенной подписи форматов CMS и CAdES.
     *
     * @param certificate массив данных
     *          Сертификат подписанта.
     *          Используется только для проверки подписи
     *          формата GOST3410.
     *
     * @param sign массив данных
     *          Подписанный документ или значение
     *          подписи для проверки.
     * @return Optional<VerificationResultRest>
     */
    @Override
    public Optional<VerificationResultRest> checkSign(byte[] target, byte[] certificate, byte[] sign ){
        Optional<VerificationResultRest> resultRest = Optional.empty();

        SignedDocument signedDocument =  new SignedDocument();
        signedDocument.setContent(sign);
        signedDocument.setSource(target);
        signedDocument.setCertificate(certificate);

        String signedDocumentStr = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            signedDocumentStr = mapper.writeValueAsString(signedDocument);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        try {
            String pathSignatures = Constant.SVS_REST + Constant.SVS_REST_SIGNATURES;
            URL url = new URL(pathSignatures);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            try(OutputStream os = con.getOutputStream()) {
                byte[] input = signedDocumentStr.getBytes("utf-8");
                os.write(input, 0, input.length);
            }catch (Exception e){
                e.printStackTrace();
            }

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                String s = response.toString();

                //Возврат происходит с квадратными скобками на первой и последней позиции
                s = s.substring(1, s.length()-1);

                resultRest = Optional.of(mapper.readValue(s, VerificationResultRest.class));
            }catch (Exception e){
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultRest;
    }
}
