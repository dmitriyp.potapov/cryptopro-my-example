package ru.potapov.crypto.cryptopro.service;

import com.google.common.io.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.CryptoPro.Crypto.CryptoProvider;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCPRequest.GostCertificateRequest;
import ru.CryptoPro.JCSP.JCSP;
import ru.potapov.crypto.cryptopro.api.ISignatureService;
import ru.potapov.crypto.cryptopro.util.Constant;

import javax.annotation.PostConstruct;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.*;
import java.util.*;

@Service
public class SignatureService implements ISignatureService {
    private Security security;
    private KeyPair keyPair;
    @Autowired
    private KeyPairGeneratorService keyPairGeneratorService;

    public KeyPair getKeyPair() {
        return keyPair;
    }

    @PostConstruct
    public void initCertificateStorage() throws Exception {
        security.addProvider(new JCP());
        security.addProvider(new JCSP());
        security.addProvider(new CryptoProvider());

        Optional<Certificate> optional = readCertSample(Constant.KEY_STORE_FULL_PATH, Constant.ALIAS_CERT);
        if (!optional.isPresent()) {
            //получение сертификата и запись его в хранилище
            writeCertSample(Constant.SIGN_KEY_PAIR_ALGALGORITHM, Constant.ALGORITHM_SIGNATURE,
                    Constant.ALIAS_CERT, Constant.KEY_STORE_FULL_PATH, Constant.DNNAME_CERT, true);
            System.out.println("Storage with certificate and private key " + Constant.KEY_STORE_FULL_PATH + " has been created");
        } else {
            System.out.println("Storage " + Constant.KEY_STORE_FULL_PATH + " exists");
        }
    }

    @Override
    public byte[] sign(String algoritm, PrivateKey privateKey, byte[] bytesTarget) {
        byte[] bytesSign = new byte[0];
        try {
            Signature signature = Signature.getInstance(algoritm, Constant.PROVIDER_NAME_JCSP);
            signature.initSign(privateKey);

            try {
                signature.update(bytesTarget);
                bytesSign = signature.sign();
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytesSign;
    }

    @Override
    public boolean checkSign(String algoritm, PublicKey publicKey, byte[] signature, byte[] target) {
        boolean verifySign = false;
        try {
            Signature signature1Verify = Signature.getInstance(algoritm, Constant.PROVIDER_NAME_JCSP);
            signature1Verify.initVerify(publicKey);
            try {
                signature1Verify.update(target);
                verifySign = signature1Verify.verify(signature);
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return verifySign;
    }

    @Override
    public PrivateKey getPrivateKey(String storePath, String alias) throws Exception {
        final KeyStore keyStore = getKeyStore(Constant.KEYSTORE_TYPE_JCSP);
        final File file = new File(storePath);

        if (!file.exists())
            throw new FileNotFoundException();

        keyStore.load(new FileInputStream(file), Constant.STORE_PASS);

        return (PrivateKey) keyStore.getKey(alias, Constant.STORE_PASS);
    }

    @Override
    public PublicKey getPublicKey(String storePath, String alias) throws Exception {
        final KeyStore keyStore = getKeyStore(Constant.KEYSTORE_TYPE_JCSP);
        final File file = new File(storePath);

        if (!file.exists())
            throw new FileNotFoundException();

        keyStore.load(new FileInputStream(file), Constant.STORE_PASS);
        Certificate certificate = keyStore.getCertificate(alias);

        PublicKey publicKey = null;
        if (certificate != null)
            publicKey = certificate.getPublicKey();

        return publicKey;
    }

    /**
     * Пример генерирования запроса, отправки запроса центру сертификации и записи
     * полученного от центра сертификата в хранилище доверенных сертификатов
     *
     * @param keyAlg    Алгоритм ключа.
     * @param signAlg   Алгоритм подписи.
     * @param alias     Алиас ключа для сохранения.
     * @param storePath Путь к хранилищу сертификатов.
     * @param dnName    DN-имя сертификата.
     * @throws Exception /
     */
    public void writeCertSample(String keyAlg, String signAlg,
                                String alias, String storePath, String dnName, boolean forDocker) throws Exception {
        // определение пути к файлу для сохранения в него содержимого хранилища
        File file = new File(storePath);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }

        /* Запись полученного от центра сертификата*/
        // инициализация хранилища доверенных сертификатов именем ключевого носителя
        // (жесткий диск)
        KeyStore keyStore = getKeyStore(Constant.KEYSTORE_TYPE_JCSP);

        // загрузка содержимого хранилища (предполагается, что инициализация
        // хранилища именем CertStoreName производится впервые, т.е. хранилища
        // с таким именем пока не существует)
        keyStore.load(null, null);

        if (!forDocker){

        /* Генерирование ключевой пары в соответствии с которой будет создан запрос
        на сертификат*/
            KeyPair keypair = keyPairGeneratorService.genKey(keyAlg, alias);
            keyPair = keypair;

            // отправка запроса центру сертификации и получение от центра
            // сертификата в DER-кодировке
            byte[] encoded = createRequestAndGetCert(keyPair, signAlg, Constant.PROVIDER_NAME_JCSP, dnName);

            // инициализация генератора X509-сертификатов
            CertificateFactory cf = CertificateFactory.getInstance(Constant.CF_ALG);
            // генерирование X509-сертификата из закодированного представления сертификата
            Certificate cert =
                    cf.generateCertificate(new ByteArrayInputStream(encoded));

            // запись сертификата в хранилище доверенных сертификатов
            // (предполагается, что на носителе с именем CertStoreName не существует
            // ключа с тем же именем alias)
            keyStore.setKeyEntry(alias, keyPair.getPrivate(), null, new Certificate[]{cert});
            keyStore.setCertificateEntry(alias, cert);
        }

        // сохранение содержимого хранилища в файл
        keyStore.store(new FileOutputStream(file), Constant.STORE_PASS);
    }

    /**
     * Пример чтения сертификата из хранилища и записи его в файл
     *
     * @param storePath Путь к хранилищу сертификатов.
     * @param alias     Алиас ключа подписи.
     * @throws Exception /
     */
    private Optional<Certificate> readCertSample(String storePath, String alias) throws Exception {
        /* Чтение сертификата их хранилища доверенных сертификатов */
        // инициализация хранилища доверенных сертификатов именем ключевого носителя
        // (жесткий диск)

        final KeyStore keyStore = getKeyStore(Constant.KEYSTORE_TYPE_JCSP);
        // определение пути к файлу для чтения содержимого хранилища
        // и последующего его сохранения
        final File file = new File(storePath);
        if (!file.exists()) {
            return Optional.empty();
        }
        // загрузка содержимого хранилища (предполагается, что хранилище,
        // проинициализированное именем CertStoreName существует)
        keyStore.load(new FileInputStream(file), Constant.STORE_PASS);

        // чтение сертификата из хранилища доверенных сертификатов
        // (предполагается, что на носителе с именем CertStoreName не существует
        // ключа с тем же именем alias)
        final Certificate cert = keyStore.getCertificate(alias);

        // сохранение содержимого хранилища в файл с тем же паролем
        keyStore.store(new FileOutputStream(file), Constant.STORE_PASS);
        return Optional.ofNullable(cert);
    }

    /**
     * Функция формирует запрос на сертификат, отправляет запрос центру сертификации
     * и получает от центра сертификат.
     *
     * @param pair              ключевая пара. Открытый ключ попадает в запрос на сертификат,
     *                          секретный ключ для подписи запроса.
     * @param signAlgorithm     Алгоритм подписи.
     * @param signatureProvider Провайдер подписи.
     * @param dnName            DN-имя сертификата.
     * @return сертификат в DER-кодировке
     * @throws Exception errors
     */
    private byte[] createRequestAndGetCert(KeyPair pair, String signAlgorithm,
                                           String signatureProvider, String dnName) throws Exception {

        // формирование запроса
        GostCertificateRequest request = createRequest(pair,
                signAlgorithm, signatureProvider, dnName);

        // отправка запроса центру сертификации и получение от центра
        // сертификата в DER-кодировке
        return request.getEncodedCert(Constant.HTTP_ADDRESS);
    }

    /**
     * Функция формирует запрос на сертификат.
     *
     * @param pair              ключевая пара. Открытый ключ попадает в запрос на сертификат,
     *                          секретный ключ для подписи запроса.
     * @param signAlgorithm     Алгоритм подписи.
     * @param signatureProvider Провайдер подписи.
     * @param dnName            DN-имя сертификата.
     * @return запрос
     * @throws Exception errors
     */
    private GostCertificateRequest createRequest(KeyPair pair, String signAlgorithm,
                                                 String signatureProvider, String dnName) throws Exception {
        /* Генерирование запроса на сертификат в соответствии с открытым ключом*/
        // создание генератора запроса на сертификат
        GostCertificateRequest request = new GostCertificateRequest(signatureProvider);
        // инициализация генератора
        // @deprecated с версии 1.0.48
        // вместо init() лучше использовать setKeyUsage() и addExtKeyUsage()
        // request.init(KEY_ALG);

    /*
    Установить keyUsage способ использования ключа можно функцией
    setKeyUsage. По умолчанию для ключа подписи, т.е. для указанного в первом
    параметре функции init() алгоритма "GOST3410EL" используется комбинация
    DIGITAL_SIGNATURE | NON_REPUDIATION. Для ключа шифрования, т.е. для
    алгоритма "GOST3410DHEL" добавляется KEY_ENCIPHERMENT | KEY_AGREEMENT.
    */
        final String keyAlgorithm = pair.getPrivate().getAlgorithm();
        if (keyAlgorithm.equalsIgnoreCase(JCP.GOST_EL_DEGREE_NAME) ||
                keyAlgorithm.equalsIgnoreCase(JCP.GOST_EL_2012_256_NAME) ||
                keyAlgorithm.equalsIgnoreCase(JCP.GOST_EL_2012_512_NAME)) {
            int keyUsage = GostCertificateRequest.DIGITAL_SIGNATURE |
                    GostCertificateRequest.NON_REPUDIATION;
            request.setKeyUsage(keyUsage);
        } else {
            int keyUsage = GostCertificateRequest.DIGITAL_SIGNATURE |
                    GostCertificateRequest.NON_REPUDIATION |
                    GostCertificateRequest.KEY_ENCIPHERMENT |
                    GostCertificateRequest.KEY_AGREEMENT;
            request.setKeyUsage(keyUsage);
        }

    /*
    Добавить ExtendedKeyUsage можно так. По умолчанию для ключа подписи,
    т.е. для алгоритма "GOST3410EL" список будет пустым. Для ключа
    шифрования, т.е. для алгоритма "GOST3410DHEL" добавляется OID
    INTS_PKIX_CLIENT_AUTH "1.3.6.1.5.5.7.3.2", а при установленном в true
    втором параметре функции init() еще добавляется INTS_PKIX_SERVER_AUTH
    "1.3.6.1.5.5.7.3.1"
    */
        request.addExtKeyUsage(GostCertificateRequest.INTS_PKIX_EMAIL_PROTECTION);

        // определение параметров и значения открытого ключа
        request.setPublicKeyInfo(pair.getPublic());
        // определение имени субъекта для создания запроса
        request.setSubjectInfo(dnName);
        // подпись сертификата на закрытом ключе и кодирование запроса
        request.encodeAndSign(pair.getPrivate(), signAlgorithm);

        return request;
    }

    /**
     * Проверка подписи на открытом ключе
     *
     * @param aliasCert псевданим сертификата
     * @param data      подписываемые данные
     * @param signature подпись
     * @return true - верна, false - не верна
     * @throws Exception /
     */
    public boolean verify(String aliasCert, byte[] data, byte[] signature) throws Exception {
        return verify(aliasCert, getPublicKey(Constant.KEY_STORE_FULL_PATH, aliasCert), data, signature);
    }

    private boolean verify(String aliasCert, PublicKey publicKey,
                           byte[] data, byte[] signature) throws Exception {

        if (publicKey == null)
            return false;

        final Signature sig = Signature.getInstance(Constant.ALGORITHM_SIGNATURE);
        sig.initVerify(publicKey);
        sig.update(data);

        return sig.verify(signature);
    }

    /**
     * Загрузка сертификата из файла в хранилище.
     *
     * @param nameCert - имя файла сертификатаи
     * @return alias загруженного сертификата
     */
    public String downloadCert(String nameCert) {
        String alias = UUID.randomUUID().toString();
        //Путь до сертификата, который надо загрузить
        String path = Constant.KEY_STORE_PATH + "/"+nameCert+".cer";
        File fileCert = new File(path);
        if (!fileCert.exists())
            return null;

        try (FileInputStream fio = new FileInputStream(fileCert)) {
            byte[] bytes = new byte[fio.available()];

            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = (byte) fio.read();
            }

            CertificateFactory cf = CertificateFactory.getInstance(Constant.CF_ALG);
            Certificate cert = cf.generateCertificate(new ByteArrayInputStream(bytes));

            //Путь до хранилища сертификатов
            File fileStore = new File(Constant.KEY_STORE_FULL_PATH);
            if (!fileStore.exists())
                return null;

            putCertToStore(cert, alias);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return alias;
    }

    /**
     * Загрузка сертификата.
     *
     * @param certStr - сертификат, помещаемый в хранидище в формате <String>
     * @return alias загруженного сертификата
     */
    @Override
    public String putCert(String certStr) {
        byte[] encoded = Constant.hexStringToByteArray(certStr);
        return putCert(encoded);
    }

    /**
     * Загрузка сертификата.
     *
     * @param encoded - - сертификат, помещаемый в хранилище в формате <byte[]>
     * @return alias загруженного сертификата
     */
    @Override
    public String putCert(byte[] encoded) {
        String alias = UUID.randomUUID().toString();

        Certificate cert = null;
        try {
            CertificateFactory cf = CertificateFactory.getInstance(Constant.CF_ALG);
            cert = cf.generateCertificate(new ByteArrayInputStream(encoded));
            putCertToStore(cert, alias);
            return alias;
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Получить сертификат.
     *
     * @param certStr - сертификат, находящийся в хранилище в формате <String>
     * @return alias загруженного сертификата
     */
    @Override
    public byte[] getCert(String certStr) {
        byte[] encoded = null;
        Optional<Certificate> optional = Optional.empty();
        try {
            optional = readCertSample(Constant.KEY_STORE_FULL_PATH, certStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (optional.isPresent()){
            encoded = certStr.getBytes();
        }
        return encoded;
    }


    /**
     * Загрузка сертификата.
     *
     * @param cert - сертификат, добавляемый в хранилище
     * @param alias - идентификатор сертификата в хранилище
     * @return true - загрузили, false - не загрузили
     */
    private boolean putCertToStore(Certificate cert, String alias){
        try {
            //Путь до хранилища сертификатов
            File fileStore = new File(Constant.KEY_STORE_FULL_PATH);
            if (!fileStore.exists())
                return false;

            KeyStore trustStore = getKeyStore(Constant.CERT_STORE_NAME_JCSP);
            trustStore.load(new FileInputStream(fileStore), Constant.STORE_PASS);

            //удаляем если уже существует
            if (trustStore.containsAlias(alias)) {
                trustStore.deleteEntry(alias);
            }

            trustStore.setCertificateEntry(alias, cert);
            // сохранение содержимого хранилища в файл
            trustStore.store(new FileOutputStream(fileStore), Constant.STORE_PASS);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    /**
     * проверка сертификата на валидность.
     *
     * @param certStr - сертификат для проверки, в формате <String>
     * @return true - сертификат валидный, false - сертификат не валидный
     */
    @Override
    public boolean valid(String certStr) {

        byte[] encoded = Constant.hexStringToByteArray(certStr);

        return valid(encoded);
    }

    /**
     * проверка сертификата на валидность.
     *
     * @param encoded - сертификат для проверки, в формате <byte[]>
     * @return true - сертификат валидный, false - сертификат не валидный
     */
    @Override
    public boolean valid(byte[] encoded) {
        //для проверки цепочки online
        //System.setProperty("com.sun.security.enableCRLDP", "true");//если используется Oracle JVM
        //для проверки цепочки
        try {
            CertificateFactory cf = CertificateFactory.getInstance(Constant.CF_ALG);
            X509Certificate cert = (X509Certificate)cf.generateCertificate(new ByteArrayInputStream(encoded));

            // Проверка валидности на дату
            cert.checkValidity();

            //Проверка цепочки
            //Путь до хранилища сертификатов
            File fileStore = new File(Constant.KEY_STORE_FULL_PATH);
            if (!fileStore.exists())
                return false;

            CertPathValidator validator = CertPathValidator.getInstance("PKIX");//CPPKIX

            /* Givens. */
            InputStream trustStoreInput = new FileInputStream(fileStore);
            char[] password = Constant.STORE_PASS;
            List<Certificate> chain = getChainCert(cert);

            // Список CRL.
            List<X509CRL> crlList = getCrls(Constant.CRL_FULL_PATH);

            /* Construct a valid path. */
            chain.add(cert);
            KeyStore anchors = getKeyStore(Constant.CERT_STORE_NAME_JCSP);
            anchors.load(trustStoreInput, password);
            X509CertSelector target = new X509CertSelector();
            target.setCertificate((X509Certificate) chain.get(0));

            PKIXBuilderParameters params = new PKIXBuilderParameters(anchors, target);

            CertStoreParameters intermediates = new CollectionCertStoreParameters(chain);
            params.addCertStore(CertStore.getInstance("Collection", intermediates));

            CertStoreParameters revoked = new CollectionCertStoreParameters(crlList);
            params.addCertStore(CertStore.getInstance("Collection", revoked));

            CertPathBuilder builder = CertPathBuilder.getInstance("PKIX");//CPPKIX
            /*
             * If build() returns successfully, the certificate is valid. More details
             * about the valid path can be obtained through the PKIXBuilderResult.
             * If no valid path can be found, a CertPathBuilderException is thrown.
             */
            PKIXCertPathBuilderResult r = (PKIXCertPathBuilderResult) builder.build(params);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * получить цепочку сертификатов от переданного сертификата до корневого.
     *
     * @param cert - сертификат для посторения цепочки
     * @return List<Certificate> сертификатов цепочки переданного сертификата
     */
    private List<Certificate> getChainCert(X509Certificate cert){
        String newAlias = cert.getSubjectDN().getName();
        putCertToStore(cert, newAlias);

        List<Certificate> chain = new ArrayList<>();
        chain.add(cert);

        KeyStore keyStore = getKeyStore(Constant.CERT_STORE_NAME_JCSP);
        final File file = new File(Constant.KEY_STORE_FULL_PATH);
        if (!file.exists())
            return chain;

        try {
            // загрузка содержимого хранилища (предполагается, что хранилище,
            // проинициализированное именем CertStoreName существует)
            keyStore.load(new FileInputStream(file), Constant.STORE_PASS);
            Certificate[] chainArray = keyStore.getCertificateChain(newAlias);
            if (chainArray != null)
                chain =  Arrays.asList( );
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }

        return chain;
    }

    /**
     * получить хранилище ключей/сертификатов по его типу
     *
     * @param storeName - тип хранилища
     * @return хранилище ключей/сертификатов, null - если такого хранилища нет
     */
    private KeyStore getKeyStore(String storeName){
        KeyStore keyStore = null;

        try {
            keyStore = KeyStore.getInstance(storeName, Constant.PROVIDER_NAME_JCSP);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }

        return keyStore;
    }

    /**
     * получить цепочку сертификатов от переданного сертификата до корневого.
     *
     * @param crlPath - путь до папки с отозванными сертификатами
     * @return List<X509CRL> отозванных сертификатов
     */
    private List<X509CRL> getCrls(String crlPath){
        List<X509CRL> crlList = new ArrayList<>();
        // Если файл с CRL есть, то читаем его.
        File dirCrl = new File(Constant.CRL_FULL_PATH);
        if (dirCrl.exists() && dirCrl.isDirectory()){
            Collection<X509CRL> crl = null;
            try {
                for (File fileCrl : dirCrl.listFiles()) {
                    if ("crl".equals( Files.getFileExtension(fileCrl.getName()) )){
                        crl = (Collection<X509CRL>) CertificateFactory.getInstance(Constant.CF_ALG).generateCRLs(new FileInputStream(fileCrl));
                        crlList.addAll(crl);
                    }
                }
            } catch (CRLException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else
            System.out.println("Нет папки с отозванными сертификатами! \nПроверка на СОС не будет осуществленна!");

        return crlList;
    }
}
