FROM ubuntu:18.04
ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /app

COPY /libs/cryptopro/* /

EXPOSE 8081

#CMD ["/install.sh"]

RUN apt-get update -q \
    && apt-get install -y openjdk-11-jdk-headless \
    && chmod +x /install.sh && /install.sh

COPY /build/libs/ms-driver-crypto-pro-example* drivercryptopro.jar
ENTRYPOINT exec java $JAVA_OPTS -jar drivercryptopro.jar